### Problem
<!-- What’s the problem that this icon solves? Why is it worth solving? --> 

### Solution
<!-- What’s the solution and why is it like that? Is the icon properly associated with the behavior it describes? Is there an existing icon in use that might conflict with this new one? Does the icon require any variants; for example, an outlined version, or versions showing status, new / opened / closed, etc,? -->

### Example(s)
<!-- One or more images showing the icon in use. -->

### Checklist

Make sure these are completed before closing the issue, with a link to the
relevant commit or issue, if applicable. Get familiar with the [icon
contribution
guidelines](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#icons).

1. [ ] **Author**: Create a Sketch file in your progress folder just for this
   icon.
1. [ ] **Author**: Ask another Product Designer to review your personal Sketch
   file, linking them to your latest commit so they know where to find it. If
   they have the capacity, they should assign themselves to this issue. If not,
   try pinging another person.
1. [ ] **Reviewer**: Review and approve icon in the author's personal Sketch
   file, according to the [workflow](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#sketch-workflow).
1. [ ] **Author**: Add icon to the GitLab Sketch UI Kit (pattern library and/or
   instance sheet), following this [step-by-step
   process](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#when-changes-are-approved).
1. [ ] **Author**: Ask the reviewer to review icon in the Sketch UI Kit files.
1. [ ] **Reviewer**: Review and approve icon in the Sketch UI Kit files,
   according to the [workflow](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#sketch-workflow).
1. [ ] **Author**: [Create an issue in the SVGs
   library](https://gitlab.com/gitlab-org/gitlab-svgs/issues/new) to add the
   icon. Mark it as related to this issue.
1. [ ] **Author**: Create a new MR and follow the [necessary steps](https://gitlab.com/gitlab-org/gitlab-svgs#exporting-icons-from-sketch) to add the icon to the SVGs library.
    1. [ ] Assign the MR to a maintainer of the project for review.
1. [ ] **Author**: Add a read only (FYI) agenda item to the next [UX weekly
   call](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit?usp=sharing)
   to inform everyone of the new icon, linking to this issue.

### Links / references

<!-- Add external links and references if necessary -->

/label ~"UX" ~"Sketch UI Kit" ~"Pajamas" ~"icon"

/cc @gitlab-com/gitlab-ux
