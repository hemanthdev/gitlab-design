<!--Add a short description of the component. If it’s helpful, add a checklist of variations
and states to the description so that a reviewer can be sure to cross reference everything
that has been completed.-->

<!--Make sure that “anyone with the link” can view.-->
[View component in Figma →](ADD LINK TO FIGMA FRAME)

### Checklist

Make sure the following are completed before closing the issue:

1. [ ] **Assignee**: Create component in your own draft file in Figma using the
[component template](https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/%5BComponent%5D),
including all variations and states.
1. [ ] **Assignee**: Ask another Figma Pilot Designer to review your component.
1. [ ] **Reviewer**: Review and approve assignee’s addition. Ensure that component
matches Sketch specs, includes all current variations and states, and (if applicable)
is responsive.
1. [ ] **Assignee**: Add notes about the component construction to the 
[Figma component build notes](https://gitlab.com/gitlab-org/gitlab-design/issues/778)
issue description. Check off the component when complete.
1. [ ] **Assignee**: Add the component to the **Globals - beta** file, and view
the component in the Assets panel to ensure it aligns with what’s outlined in the
[Figma structure for library](https://gitlab.com/gitlab-org/gitlab-design/issues/791) issue.
1. [ ] **Assignee**: Publish the library changes along with a clear commit message.

---

/label ~"UX" ~"Figma"
